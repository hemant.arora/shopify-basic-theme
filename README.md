# Shopify Basic Theme

**Author:** Hemant Arora
**Company:** TivLabs

This is a basic Shopify skeleton theme for development.

Templates included:

- Article,
- Blog,
- Cart,
- Collection,
- Index,
- Page,
- Product,
- Search, and
- Templates for all customer pages


Key features:

- Cart page features a shipping calculator
- Nested (dropdown-enabled) menus
- Blog sidebar with recent posts and tag-cloud
- Sliders and carousels implemented
- Bootstrap-enabled layout
- FontAwesome-enabled form buttons and links
- Useful scripts like TouchSwipe.js, OwlCarousel, Selectize and cookie.js included

